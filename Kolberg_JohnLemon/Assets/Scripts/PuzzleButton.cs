﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleButton : MonoBehaviour
{
    public GameObject player;
    public ButtonController controller;
    public int id;
   public bool pressed;

    private void OnTriggerEnter(Collider other)
   {
        if (other.gameObject == player && pressed == false)
       {
            print("STEPPED ON: " + id);
            pressed = true;
            transform.position = transform.position + new Vector3(0f, -.1f, 0f);
            controller.ButtonPressed(id);
            //gameObject.SetActive(false);

            
       }

   }
}


