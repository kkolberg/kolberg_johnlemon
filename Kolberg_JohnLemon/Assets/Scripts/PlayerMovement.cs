﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    float sprintEnergy = 1f;

    public GameObject noteHolder;
    public GameObject wallToDestroy;
    public Text noteText;
    public Text countText;
    public Text finishText;
    public bool noteOpened;
    private int keys;
    private int count;
   
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        keys = 0;
        count = 0;
        SetCountText();
        finishText.text = "";
    }

   
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        
        m_Movement.Set(horizontal, 0f, vertical);
        
        m_Movement.Normalize();

       
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;

       
        m_Animator.SetBool("isWalking", isWalking);

        /*
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        */

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);

        print(desiredForward);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
    private void OnAnimatorMove()
    {
       
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);

        m_Rigidbody.MoveRotation(m_Rotation);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && noteOpened)
        {
            CloseNotePanel();
        }

        if (Input.GetKey(KeyCode.Space) && sprintEnergy > 0f)
        {
            m_Animator.speed = 3f;
            sprintEnergy -= Time.deltaTime;
        }
        else
        {
            m_Animator.speed = 1f;
            sprintEnergy += Time.deltaTime * .25f;
            if (sprintEnergy > 1f)
                sprintEnergy = 1f;
        }
     
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Note"))
        {
            NoteScript nScript = other.gameObject.GetComponent<NoteScript>();
            OpenNotePanel(nScript.noteContents);
        }
       
           
            if (other.gameObject.CompareTag("Key"))
            {
                keys++; 
                Destroy(other.gameObject);
                 }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Chest") && keys > 0)
        {
            keys--;

            Destroy(collision.gameObject);
            count = count + 1;
            SetCountText();
        }
    }
    void OpenNotePanel(string text) 
    {
        noteOpened = true;
        Time.timeScale = 0f;

        noteText.text = text;
        noteHolder.SetActive(true);
    }

    void CloseNotePanel()
    {
        noteOpened = false;
        Time.timeScale = 1f;
        noteHolder.SetActive(false);
    }

    void SetCountText()
    {
        countText.text = "Chests Remaining: " + (3-count).ToString ();
        if (count >= 3)
        {
            finishText.text = "All Secrets Revealed!";
        }
    }
}
