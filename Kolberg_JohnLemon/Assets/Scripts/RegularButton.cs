﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularButton : MonoBehaviour
    
{
    public GameObject wallToDestroy;
    private void OnTriggerEnter(Collider other)
    {
        transform.position = transform.position + new Vector3(0f, -.1f, 0f);
        Destroy(wallToDestroy);

    }
}
