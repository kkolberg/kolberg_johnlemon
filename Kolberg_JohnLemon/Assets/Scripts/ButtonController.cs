﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public GameObject wallToDestroy;
    public PuzzleButton[] buttons;

   public List<int> responses;
    private void Start()
    {
        responses = new List<int>();
    }
    public void ButtonPressed(int id)
    {
        responses.Add(id);
        print("repsonses:");
        foreach (int numb in responses)
         print(numb);
        if (responses.Count >= 3)
        {
            //corresponding button ID numbers must be pressed in a certain order to destroy the wall
           if (responses[0]==1 && responses[1]==2 && responses[2] == 3)
            {
                Destroy(wallToDestroy);
            }
           else
            {//if the buttons are not pressed correctly, the following will happen
                foreach (PuzzleButton button in buttons)
                {
                    button.pressed = false;
                    //buttons will sink down into the floor when pressed
                    button.transform.position = button.transform.position + new Vector3(0f, .1f, 0f);
                }
                /*buttons return to their original state, allowing the player to try the puzzle again
                without restarting the game*/
                responses.Clear();
            }
        }
    }

   
}
